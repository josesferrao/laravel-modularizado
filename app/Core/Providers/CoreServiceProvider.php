<?php

namespace App\Core\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Barryvdh\Debugbar\ServiceProvider as DebugbarServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Blade::directive('datetime', function ($expression) {
            return "<?php echo with{$expression}->format('m/d/Y H:i'); ?>";
        });

        Blade::directive('year', function () {
            return "<?php echo date('Y'); ?>";
        });

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'core');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() != 'production' and class_exists(DebugbarServiceProvider::class)) {
            $this->app->register(DebugbarServiceProvider::class);
        }
    }
}
