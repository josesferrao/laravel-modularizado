<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    @yield('head')
</head>
<body>
    @yield('body')
</body>
    @yield('script')
</html>