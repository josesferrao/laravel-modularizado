<?php

namespace App\Applications\Site\Providers;

use App\Core\Providers\AbstractApplicationServiceProvider;
use Illuminate\Contracts\Routing\Registrar as Router;

class SiteServiceProvider extends AbstractApplicationServiceProvider
{
    protected $path = 'Applications/Site';

    public function boot()
    {
        $this->loadViewsFrom(app_path($this->path).'/resources/views', 'site');
    }

    /**
     * Register application routes.
     *
     * @param Router $router
     * @return void
     */
    protected function registerRoutes(Router $router)
    {
        $attributes = [
            'namespace' => 'App\Applications\Site\Http\Controllers',
        ];

        $router->group($attributes, function ($router) {
            require app_path($this->path).'/Http/routes.php';
        });
    }
}
