<?php

/** @var \Illuminate\Routing\Router $router */

$router->get('/', ['as' => 'site.index', 'uses' => 'SiteController@index']);


$router->get('start', function (){
	return view('site::base');
});

