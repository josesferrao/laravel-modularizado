<?php

namespace App\Applications\Site\Http\Controllers;

use Artesaos\SEOTools\Traits\SEOTools;
use App\Core\Http\Controllers\CoreController;
use App\Support\Traits\UseFlashTrait;

class BaseController extends CoreController
{
    use UseFlashTrait, SEOTools;

    protected $view_namespace = 'site::';
}
