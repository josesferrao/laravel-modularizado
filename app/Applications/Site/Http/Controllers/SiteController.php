<?php

namespace App\Applications\Site\Http\Controllers;

class SiteController extends BaseController
{
    /**
     * @return \
     */
    public function index()
    {
        return view('site::base');
    }
}
